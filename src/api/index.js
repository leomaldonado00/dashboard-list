import axios from 'axios';

const BASE_URL = 'https://webhooks.multifiber.cl/api/v1/auth/obtain_token/';

const apiCall = (url, data, headers, method) =>
  axios({
    method,
    url: BASE_URL + url,
    data,
    headers,
    timeout: 300000000000,
  });

export { apiCall };
