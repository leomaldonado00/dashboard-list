import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import Login from "./access/Login";
import CreateDashboard from "./dashboard/CreateDashboard";
import DashboardDetail from "./dashboard/DashboardDetail";
import DashboardList from "./dashboard/DashboardList";
import EditDashboard from "./dashboard/EditDashboard";

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Login} />
        <PrivateRoute
          exact
          path="/create-dashboard"
          component={CreateDashboard}
        />
        <PrivateRoute exact path="/dashboard-list" component={DashboardList} />
        <PrivateRoute
          exact
          path="/dashboard-detail/:id"
          component={DashboardDetail}
        />
        <PrivateRoute
          exact
          path="/edit-dashboard/:id"
          component={EditDashboard}
        />
      </Switch>
    </Router>
  );
};

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        localStorage.getItem("token") ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/" }} />
        )
      }
    />
  );
};

export default Routes;
