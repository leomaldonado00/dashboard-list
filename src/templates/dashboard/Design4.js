import React from "react";
import styled from "styled-components";

const Design4 = (props) => {
  const { dashboardDetail } = props;
  return (
    <BoxWrapDesign>
      <Box>
        {dashboardDetail.url_1 ? (
          <iframe
            src={dashboardDetail.url_1}
            width="auto"
            height="auto"
            title="Iframe Example2"
            id="inlineFrameExample2"
          ></iframe>
        ) : null}
      </Box>

      <WrapTwo>
        <Box>
          {dashboardDetail.url_2 ? (
            <iframe
              src={dashboardDetail.url_2}
              width="auto"
              height="auto"
              title="Iframe Example2"
              id="inlineFrameExample2"
            ></iframe>
          ) : null}
        </Box>
        <Box>
          {dashboardDetail.url_3 ? (
            <iframe
              src={dashboardDetail.url_3}
              width="auto"
              height="auto"
              title="Iframe Example3"
              id="inlineFrameExample2"
            ></iframe>
          ) : null}
        </Box>
      </WrapTwo>
      <Box>
        {dashboardDetail.url_4 ? (
          <iframe
            src={dashboardDetail.url_4}
            width="auto"
            height="auto"
            title="Iframe Example4"
            id="inlineFrameExample4"
          ></iframe>
        ) : null}
      </Box>
    </BoxWrapDesign>
  );
};

const BoxWrapDesign = styled.div`
  min-height: 70vh;
  // max-width: 450px;
  // height:250px;
  margin: 0px auto;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
const Box = styled.div`
  margin: 6px auto 0px auto;
  border: 1px solid #c8c8d0;
  // width: 100%;
  // min-height: 100vh;
  background: #00beb6; //
  display: flex;
  font-size: 30px;
  text-align: center;
  justify-content: center;
  align-items: center;
`;
const WrapTwo = styled.div`
  // width:90%;
  // height:50px;
  margin: auto;
  display: flex;
  width: 90%;
  flex-wrap: wrap;

  justify-content: center;
  align-items: center;
  // justify-content: center;
  @media (min-width: 992px) {
    width: 100%;
  }
`;

export default Design4;
