import React, { useState } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { source } from "../../ducks/dashboard";
import Form5 from "./Form5";
import Form2 from "./Form2";
import Form3 from "./Form3";
import Form4 from "./Form4";
import LayoutDashboard from "../../components/LayoutDashboard";

const EditDashboard = ({ match }) => {
  const sourceTemp = useSelector((state) => source(state));

  const { id } = match && match.params ? match.params : null; // id de patient

  const dashboardDetail =
    sourceTemp && sourceTemp.dashboards && sourceTemp.dashboards[id]
      ? sourceTemp.dashboards[id]
      : null;

  const [formLogin, setFormLogin] = useState({
    design: `${
      dashboardDetail && dashboardDetail.design ? dashboardDetail.design : ""
    }`,
    Edesign: "",
  });

  const escrib = (e) => {
    if (e.target.name === "design") {
      setFormLogin({
        ...formLogin,
        [e.target.name]: e.target.value,
        [`E${e.target.name}`]: "",
      });
    }
  };

  return (
    <LayoutDashboard>
      <Title color="rgb(0, 190, 182)">
        Editar Dashboard {dashboardDetail.id}
      </Title>

      <Title color="rgb(74, 87, 99)" fontSize="14px" margin="12px auto">
        Seleccione una opción de presentación:
      </Title>

      <WrapLabel>
        <Label color="rgb(74, 87, 99)" fontSize="14px" margin="0px 12px">
          <input
            type="radio"
            value="2"
            name="design"
            onChange={(e) => escrib(e)}
          />
          Opcion 1
        </Label>
        <Label color="rgb(74, 87, 99)" fontSize="14px" margin="0px 12px">
          <input
            type="radio"
            value="3"
            name="design"
            onChange={(e) => escrib(e)}
          />
          Opcion 2
        </Label>
        <Label color="rgb(74, 87, 99)" fontSize="14px" margin="0px 12px">
          <input
            type="radio"
            value="4"
            name="design"
            onChange={(e) => escrib(e)}
          />
          Opcion 3
        </Label>
        <Label color="rgb(74, 87, 99)" fontSize="14px" margin="0px 12px">
          <input
            type="radio"
            value="5"
            name="design"
            onChange={(e) => escrib(e)}
          />
          Opcion 4
        </Label>
      </WrapLabel>

      {formLogin.design === "2" ? (
        <Form2
          dashboardDetail={dashboardDetail ? dashboardDetail : null}
          action="edit"
        />
      ) : null}
      {formLogin.design === "3" ? (
        <Form3
          dashboardDetail={dashboardDetail ? dashboardDetail : null}
          action="edit"
        />
      ) : null}
      {formLogin.design === "4" ? (
        <Form4
          dashboardDetail={dashboardDetail ? dashboardDetail : null}
          action="edit"
        />
      ) : null}
      {formLogin.design === "5" ? (
        <Form5
          dashboardDetail={dashboardDetail ? dashboardDetail : null}
          action="edit"
        />
      ) : null}
    </LayoutDashboard>
  );
};

const Title = styled.div`
  font-family: ${(props) => props.fontFamily || "Montserrat, sans-serif"};
  font-size: ${(props) => props.fontSize || "28px"};
  font-weight: ${(props) => props.fontWeight || "300"};
  line-height: ${(props) => props.lineHeight || "normal"};
  text-align: ${(props) => props.textAlign || "center"};
  color: ${(props) => props.color || "silver"};
  margin: ${(props) => props.margin || "auto"};
`;
const Label = styled.label`
  font-family: ${(props) => props.fontFamily || "Montserrat, sans-serif"};
  font-size: ${(props) => props.fontSize || "28px"};
  font-weight: ${(props) => props.fontWeight || "300"};
  line-height: ${(props) => props.lineHeight || "normal"};
  text-align: ${(props) => props.textAlign || "center"};
  color: ${(props) => props.color || "silver"};
  margin: ${(props) => props.margin || "auto"};
`;
const WrapLabel = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  // background: #f6f6f6; //
  flex-wrap: wrap;
`;

export default EditDashboard;
