import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";

import { source } from "../../ducks/dashboard";
import LayoutDashboard from "../../components/LayoutDashboard";
import Design5 from "./Design5";
import Design4 from "./Design4";

const DashboardDetail = ({ match }) => {
  const sourceTemp = useSelector((state) => source(state));

  const { id } = match && match.params ? match.params : null; // id de patient
  const dashboardDetail =
    sourceTemp && sourceTemp.dashboards && sourceTemp.dashboards[id]
      ? sourceTemp.dashboards[id]
      : null;

  return (
    <LayoutDashboard>
      <Title color="rgb(0, 190, 182)" margin="0 0 40px 0">
        Dashboard {dashboardDetail.id}
      </Title>

      {dashboardDetail.design === "2" ? (
        <Design5 dashboardDetail={dashboardDetail ? dashboardDetail : null} />
      ) : null}
      {dashboardDetail.design === "3" ? (
        <Design4 dashboardDetail={dashboardDetail ? dashboardDetail : null} />
      ) : null}
      {dashboardDetail.design === "4" ? (
        <Design4 dashboardDetail={dashboardDetail ? dashboardDetail : null} />
      ) : null}
      {dashboardDetail.design === "5" ? (
        <Design5 dashboardDetail={dashboardDetail ? dashboardDetail : null} />
      ) : null}
    </LayoutDashboard>
  );
};

const Title = styled.div`
  font-family: ${(props) => props.fontFamily || "Montserrat, sans-serif"};
  font-size: ${(props) => props.fontSize || "28px"};
  font-weight: ${(props) => props.fontWeight || "300"};
  line-height: ${(props) => props.lineHeight || "normal"};
  text-align: ${(props) => props.textAlign || "center"};
  color: ${(props) => props.color || "silver"};
  margin: ${(props) => props.margin || "auto"};
`;

export default DashboardDetail;
