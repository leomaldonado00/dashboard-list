import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { MdDashboard } from "react-icons/md";
import { FaEdit } from "react-icons/fa";
import { source } from "../../ducks/dashboard";
import LayoutDashboard from "../../components/LayoutDashboard";

const DashboardList = () => {
  const sourceTemp = useSelector((state) => source(state));
  const history = useHistory();

  return (
    <LayoutDashboard>
      <Title color="rgb(0, 190, 182)" margin="0 0 40px 0">
        Lista de Dashboard
      </Title>

      {sourceTemp &&
      sourceTemp.dashboards &&
      sourceTemp.dashboards.length > 0 ? (
        <div>
          {sourceTemp.dashboards.map((element, i) => (
            <WrapCard key={i} margin="0px auto 10px auto">
              <Notif
                onClick={() => history.push(`/dashboard-detail/${element.id}`)}
              >
                <Notif>
                  <MdDashboard size={40} color="silver" />
                </Notif>
                <Title
                  color="rgb(0, 190, 182)"
                  fontSize="23px"
                  cursor="pointer"
                  margin="0 0 0 15px"
                >
                  Dashboard {element.id}
                </Title>
              </Notif>
              <Notif
                onClick={() => history.push(`/edit-dashboard/${element.id}`)}
              >
                <FaEdit size={28} color="rgb(0,190,182)" />
              </Notif>
            </WrapCard>
          ))}
        </div>
      ) : (
        <Title color="rgb(74, 87, 99)" fontSize="14px" margin="12px auto">
          No posee dashboard creado
        </Title>
      )}
    </LayoutDashboard>
  );
};

const Title = styled.div`
  font-family: ${(props) => props.fontFamily || "Montserrat, sans-serif"};
  font-size: ${(props) => props.fontSize || "28px"};
  font-weight: ${(props) => props.fontWeight || "300"};
  line-height: ${(props) => props.lineHeight || "normal"};
  text-align: ${(props) => props.textAlign || "center"};
  color: ${(props) => props.color || "silver"};
  margin: ${(props) => props.margin || "0 0 0 0"};
  cursor: ${(props) => props.cursor || "auto"};
`;
const WrapCard = styled.div`
  width: calc(60% - 20px);
  padding: 0px 10px;
  // min-height: 100vh;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin: ${(props) => props.margin || "0 0 0 0"};
  border-radius: 10px;
  border: 1px solid rgb(0, 190, 182);
  min-height: 60px;
  height: auto;
  @media (max-width: 720px) {
    width: calc(100% - 20px);
  }
`;
const Notif = styled.div`
  cursor: ${(props) => props.cursor || "pointer"};
  // position:absolute;  /* left slf */
  width: auto;
  height: auto;
  display: flex;
  align-items: center;
  margin-right: ${(props) => props.marginRight || "0"};
  margin-left: ${(props) => props.marginLeft || "0"};
`;

export default DashboardList;
