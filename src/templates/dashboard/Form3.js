import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { createDashboard, source } from "../../ducks/dashboard";
import Button from "../../components/Button";

const Form3 = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const sourceTemp = useSelector((state) => source(state));

  const [formLogin, setFormLogin] = useState({
    url_1: `${
      props.dashboardDetail && props.dashboardDetail.url_1
        ? props.dashboardDetail.url_1
        : ""
    }`,
    url_2: `${
      props.dashboardDetail && props.dashboardDetail.url_2
        ? props.dashboardDetail.url_2
        : ""
    }`,
    url_3: `${
      props.dashboardDetail && props.dashboardDetail.url_3
        ? props.dashboardDetail.url_3
        : ""
    }`,
    design: "3",
    Eurl_1: ``,
    Eurl_2: "",
    Eurl_3: "",
    Edesign: "",
  });

  const validate = (dataLogin) => {
    let errores = {};
    if (!dataLogin.url_1 || !dataLogin.url_2 || !dataLogin.url_3) {
      if (dataLogin.url_1.length === 0) {
        errores = { ...errores, Eurl_1: "El campo es requerido", Eflag: true };
      }
      if (dataLogin.url_2.length === 0) {
        errores = { ...errores, Eurl_2: "El campo es requerido", Eflag: true };
      }
      if (dataLogin.url_3.length === 0) {
        errores = { ...errores, Eurl_3: "El campo es requerido", Eflag: true };
      }

      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }

    setFormLogin({ ...dataLogin, Eflag: false });
    return true;
  };

  const escrib = (e) => {
    if (
      e.target.name === "design" ||
      e.target.name === "url_1" ||
      e.target.name === "url_2" ||
      e.target.name === "url_3"
    ) {
      setFormLogin({
        ...formLogin,
        [e.target.name]: e.target.value,
        [`E${e.target.name}`]: "",
      });
    }
  };

  const validate0 = (dataLogin) => {
    const valid = validate(dataLogin);

    let sourceGral = JSON.stringify(sourceTemp);
    let sourceGralParse = JSON.parse(sourceGral);

    const { url_1, url_2, url_3, design } = dataLogin;
    const dataLoginF =
      props.action === "create"
        ? { url_1, url_2, url_3, design, id: sourceGralParse.dashboards.length }
        : { url_1, url_2, url_3, design, id: props.dashboardDetail.id };
    if (valid) {

      if (props.action === "create") {
        // creando ?
        sourceGralParse.dashboards.push(dataLoginF);

        dispatch(createDashboard(sourceGralParse));
      }
      if (props.action === "edit") {
        // editando ?
        // agg o reemplazar elemento
        sourceGralParse.dashboards[props.dashboardDetail.id] = dataLoginF;
        dispatch(createDashboard(sourceGralParse)); // update
      }

      history.push(`/dashboard-detail/${dataLoginF.id}`);
      // setFlag(false);
    }
  };

  return (
    <div>
      <Title color="rgb(0, 190, 182)" fontSize="20px" margin="15px auto">
        Presentación 2
      </Title>

      <BoxWrapDesign>
        <Box>1</Box>
        <WrapTwo>
          <Box>2</Box>
          <Box>3</Box>
        </WrapTwo>
      </BoxWrapDesign>

      <WrapForm>
        <>
          <Title
            color="rgb(74, 87, 99)"
            fontSize="14px"
            margin="5px auto 0px auto"
          >
            Panel 1
          </Title>
          <Input
            name="url_1"
            type="text"
            placeholder=" Url Iframe 1"
            onChange={(e) => escrib(e)}
            value={formLogin.url_1}
          />

          {formLogin.Eurl_1.length !== 0 /* || formLogin.url.length !== 0 */ ? (
            <Error>{formLogin.Eurl_1}</Error>
          ) : null}
        </>

        <>
          <Title
            color="rgb(74, 87, 99)"
            fontSize="14px"
            margin="5px auto 0px auto"
          >
            Panel 2
          </Title>
          <Input
            name="url_2"
            type="text"
            placeholder=" Url Iframe 2"
            onChange={(e) => escrib(e)}
            value={formLogin.url_2}
          />

          {formLogin.Eurl_2.length !== 0 /* || formLogin.url.length !== 0 */ ? (
            <Error>{formLogin.Eurl_2}</Error>
          ) : null}
        </>

        <>
          <Title
            color="rgb(74, 87, 99)"
            fontSize="14px"
            margin="5px auto 0px auto"
          >
            Panel 3
          </Title>
          <Input
            name="url_3"
            type="text"
            placeholder=" Url Iframe 3"
            onChange={(e) => escrib(e)}
            value={formLogin.url_3}
          />

          {formLogin.Eurl_3.length !== 0 /* || formLogin.url.length !== 0 */ ? (
            <Error>{formLogin.Eurl_3}</Error>
          ) : null}
        </>

        <Button
          text="ENVIAR"
          width="350px"
          // marginTop="50px"
          margin="30px auto 0px auto"
          heightButton="30px"
          onClick={() => {
            validate0(formLogin);
          }}
        />
      </WrapForm>
    </div>
  );
};

const Error = styled.div`
  font-family: Montserrat;
  color: red;
  font-size: 13px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  text-align: center;
`;
const Title = styled.div`
  font-family: ${(props) => props.fontFamily || "Montserrat, sans-serif"};
  font-size: ${(props) => props.fontSize || "28px"};
  font-weight: ${(props) => props.fontWeight || "300"};
  line-height: ${(props) => props.lineHeight || "normal"};
  text-align: ${(props) => props.textAlign || "center"};
  color: ${(props) => props.color || "silver"};
  margin: ${(props) => props.margin || "auto"};
`;
const Input = styled.input`
  width: 350px;
  height: 20px;
  border-radius: 5px;
  background-color: #f6f6f6;
  border: 1px solid #c8c8d0;
  margin: 2px auto;
  padding-left: 5px;
  outline: none;
  color: gray;
  ::placeholder {
    color: #43425d;
  }
  @media (max-width: 480px) {
    width: calc(100% - 7px);
  }
`;
const WrapForm = styled.div`
  display: grid;
  align-content: center;
  justify-content: center;
`;
const BoxWrapDesign = styled.div`
  max-width: 450px;
  height: 250px;
  margin: 0px auto 12px auto;
  border: 1px solid #00beb6;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media (max-width: 350px) {
    height: 200px;
  }
`;
const Box = styled.div`
  color: #ffffff;
  // max-width: 50px;
  width: 100px;
  height: 50px;
  margin: auto;
  border: 1px solid #c8c8d0;
  // min-height: 100vh;
  background: #00beb6; //
  display: flex;
  font-size: 30px;
  text-align: center;
  justify-content: center;
  align-items: center;
  // display: grid;
  // align-content: center;
`;
const WrapTwo = styled.div`
  // height:50px;
  margin: auto;
  display: flex;
  width: 90%;
`;

export default Form3;
