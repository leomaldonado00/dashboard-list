import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import {
  login,
  errorLogin,
  loadingLogin,
} from "../../ducks/access";
import { source } from "../../ducks/dashboard";
import Button from "../../components/Button";

const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const loadingTemp = useSelector((state) => loadingLogin(state));
  const errorTemp = useSelector((state) => errorLogin(state));
  const sourceTemp = useSelector((state) => source(state));

  const [formLogin, setFormLogin] = useState({
    username: ``,
    password: "",
    Eusername: "",
    Epassword: "",
  });
  const [flag, setFlag] = useState(true);

  const validate = (dataLogin) => {
    let errores = {};
    if (!dataLogin.username || !dataLogin.password) {
      if (dataLogin.username.length === 0) {
        errores = {
          ...errores,
          Eusername: "El campo es requerido",
          Eflag: true,
        };
      }
      if (dataLogin.password.length === 0) {
        errores = {
          ...errores,
          Epassword: "El campo es requerido",
          Eflag: true,
        };
      }

      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }

    if (
      dataLogin.username.length !== 0 &&
      !/^(.){3,20}$/.test(dataLogin.username)
    ) {
      errores = {
        ...errores,
        Eusername: "El username debe contener de 3 a 20 caracteres",
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }

    if (
      dataLogin.password.length !== 0 &&
      !/^(.){6,20}$/.test(dataLogin.password)
    ) {
      errores = {
        ...errores,
        Epassword: "La contraseña debe contener de 6 a 20 caracteres",
        Eflag: true,
      };
      setFormLogin({ ...dataLogin, ...errores });
      return false;
    }

    setFormLogin({ ...dataLogin, Eflag: false });
    return true;
  };

  const escrib = (e) => {
    if (e.target.name === "password" || e.target.name === "username") {
      if (e.target.value.length <= 20) {
        setFormLogin({
          ...formLogin,
          [e.target.name]: e.target.value,
          [`E${e.target.name}`]: "",
        });
        setFlag(true);
      }
    }
  };

  const validate0 = (dataLogin) => {
    const valid = validate(dataLogin);
    const { username, password } = dataLogin;
    const dataLoginF = { username, password };
    if (valid) {
      // localStorage.clear();
      dispatch(
        login(dataLoginF, (response) => {
          if (response.bool) {
            if (response.data) {
              if (
                response.data.data &&
                response.data.data.token &&
                response.data.status === 200
              ) {
                if (sourceTemp.dashboards.length > 0) {
                  // have some?
                  history.push("/dashboard-list");
                } else {
                  history.push("/create-dashboard");
                }
              }
            }
          }
        })
      );
      setFlag(false);
    }
  };

  return (
    <WrapAccess>
      <LogoLogin color="#fff" fontWeight="600">
        ACME
      </LogoLogin>
      <Title fontSize="28px">INICIAR SESIÓN</Title>

      <Input
        name="username"
        type="text"
        placeholder=" Username"
        onChange={(e) => escrib(e)}
        value={formLogin.username}
      />

      {formLogin.Eusername.length !== 0 /* || formLogin.url.length !== 0 */ ? (
        <Error>{formLogin.Eusername}</Error>
      ) : null}

      <Input
        name="password"
        type="password"
        placeholder=" Password"
        onChange={(e) => escrib(e)}
        value={formLogin.password}
      />

      {formLogin.Epassword.length !== 0 /* || formLogin.url.length !== 0 */ ? (
        <Error>{formLogin.Epassword}</Error>
      ) : null}

      {loadingTemp ? (
        <Title fontSize="20px" margin="30px auto 0px auto">
          LOADING......................
        </Title>
      ) : (
        <Button
          text="ENVIAR"
          width="350px"
          // marginTop="50px"
          margin="30px auto 0px auto"
          heightButton="40px"
          onClick={() => {
            validate0(formLogin);
          }}
        />
      )}

      {!flag && errorTemp ? <Error>{errorTemp}</Error> : null}
    </WrapAccess>
  );
};

const WrapAccess = styled.div`
  width: 100%;
  min-height: 100vh;
  background: #f6f6f6; //

  align-content: center;
  display: grid;
  justify-content: center;
`;
const Title = styled.div`
  font-family: Montserrat, sans-serif;
  font-size: ${(props) => props.fontSize || "28px"};
  font-weight: ${(props) => props.fontWeight || "300"};
  line-height: 1.71;
  text-align: center;
  color: rgb(0, 190, 182);
  margin: ${(props) => props.margin || "auto"};
`;
const LogoLogin = styled.div`
  font-family: Montserrat, sans-serif;
  font-size: ${(props) => props.fontSize || "28px"};
  font-weight: ${(props) => props.fontWeight || "300"};
  line-height: 1.71;
  text-align: center;
  color: rgb(0, 190, 182);
  margin: ${(props) => props.margin || "auto"};
  position: absolute;
  left: 15px;
  top: 15px;
`;
const Error = styled.p`
  font-family: Montserrat;
  color: red;
  font-size: 13px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.29;
  text-align: center;
`;
const Input = styled.input`
  width: 350px;
  height: 30px;
  border-radius: 5px;
  background-color: #f6f6f6;
  border: 1px solid #c8c8d0;
  margin: 8px auto;
  padding-left: 5px;
  outline: none;
  color: gray;
  ::placeholder {
    color: #43425d;
  }
  @media (max-width: 480px) {
    width: calc(100% - 7px);
  }
`;

export default Login;
