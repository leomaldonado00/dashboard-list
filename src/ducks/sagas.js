import { all } from 'redux-saga/effects';
import { sagaAccess } from './access/sagas';


export default function* rootSaga() {
  yield all([sagaAccess()]);
};
