import * as types from './types';

const login = (payload,callback) => ({
  type: types.LOGIN_START,
  payload,
  callback,
});
const logout = () => ({
  type: types.LOGOUT_START,
  payload: {},
});

export {
  login,
  logout
};
