import { get } from 'lodash';

export const userLogin = state => get(state, 'accessStore.userLogin');
export const errorLogin = state => get(state, 'accessStore.errorLogin');
export const loadingLogin = state => get(state, 'accessStore.loadingLogin');
