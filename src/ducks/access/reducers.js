import * as types from './types';

const InitialState = {};

export default function accessStore(state = InitialState, action) {
  switch (action.type) {
    case types.LOGIN_START:
      return {
        ...state,
        loadingLogin: true,
        userLogin: null,
        errorLogin: null,
      };
    case types.LOGIN_COMPLETE:
      return {
        ...state,
        userLogin: action.userLoged.userLoged.data.token,
        loadingLogin: null,
        errorLogin: null,
      };
    case types.LOGIN_ERROR:
      return {
        ...state,
        loadingLogin: null,
        errorLogin: action.error,
        userLogin: null,
      };
      case types.LOGOUT_START:
        return {
          ...state,
          loadingLogin: null,
          errorLogin: null,
          userLogin: null,  
        };

    default:
      return state;
  }
}
