import { put, call, takeLatest } from 'redux-saga/effects';
import * as types from './types';
import { apiCall } from '../../api';


export function* login({payload,callback}) { // // // // 
  try {
    const userLoged = yield call(
      apiCall,
      null,
      payload,
      null,
      'POST',
    );
    if (userLoged !== undefined && userLoged.status === 200) {
      yield localStorage.setItem('token', userLoged.data.token);
      yield put({
        type: types.LOGIN_COMPLETE,
        userLoged: { userLoged, loginFlag: true },
      });
      callback({bool:true,data:userLoged}); // // // // 
    }
  } catch (error) {
    if (error.response !== undefined) {
      if (error.response.status === 401) {
        const errorDirec = 'Requires authorization for this area';
        yield put({
          type: types.LOGIN_ERROR,
          error: errorDirec,
        });
      } else if (error.response.status === 404) {
        const errorDirec = 'This address does not exist';
        yield put({
          type: types.LOGIN_ERROR,
          error: errorDirec,
        });
      } else if (error.response.status === 500) {
        const errorServer =
          'Sorry. There has been an internal server problem';
        yield put({
          type: types.LOGIN_ERROR,
          error: errorServer,
        });
      } else {
        yield put({
          type: types.LOGIN_ERROR,
          error:
            error.response.data.non_field_errors 
        });
      }
    } else {
      const errorConex = 'CONNECTION ERROR';
      yield put({
        type: types.LOGIN_ERROR,
        error: errorConex,
      });
    }
  }
}

export function* sagaAccess() {
  yield takeLatest(types.LOGIN_START, login);
}


