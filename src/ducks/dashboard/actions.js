import * as types from './types';

  const createDashboard = (source) => ({
    type: types.CREATE_DASHBOARD_START,
    payload: { source },
  });

  export { createDashboard };

  