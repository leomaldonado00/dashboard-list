import * as types from './types';
import json from '../../json/api.json'; // initial

const InitialState = json;
export default function dashboardStore(state = InitialState, action) {
  switch (action.type) {
      case types.CREATE_DASHBOARD_START:
        return {
          ...action.payload.source,
        };    
    default:
      return state;
  }
}
