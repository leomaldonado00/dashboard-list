import { get } from 'lodash';

export const source = state => get(state, 'dashboardStore');
