import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; 
import createSagaMiddleware from 'redux-saga'; 
import accessStore from './access';
import dashboardStore from './dashboard';

const reducers = combineReducers({
  accessStore,
  dashboardStore,
});

const persistConfig = {
  key: 'dashboardList',
  storage,
};
const persistedReducer = persistReducer(persistConfig, reducers);
const sagaMiddleware = createSagaMiddleware();

export default () => {
  const store = createStore(
    persistedReducer,
    undefined,
    compose(applyMiddleware(sagaMiddleware)), 
  );
  const persistor = persistStore(store);
  return { store, persistor, sagaMiddleware };
};