import React from "react";
import styled from "styled-components";
import Header from "./Header";

const LayoutDashboard = (props) => (
  <div>
    <Header
      itemHeader={props.itemHeader}
      history={props.history}
      goToId={props.goToId}
      arrow={props.arrow}
      bg="rgb(0, 190, 182)"
    />
    <Children>
      <GrandChildren>{props.children}</GrandChildren>
    </Children>
  </div>
);

const Children = styled.div`
  width: 100%;
  min-height: calc(100vh - 64px); // headerpx
  // height: 100vh;
  margin-top: 64px;
  margin-bottom: 0;
  margin-left: 0;
  margin-right: 0;
  background: #f6f6f6;
  /* margin-bottom: 20vh; */
`;
const GrandChildren = styled.div`
  width: 90%;
  max-width: 1200px;
  min-height: calc(100vh - 94px); // headerpx + paddingpx
  // height: 100vh;
  // margin-top: 94px;
  // margin-bottom:0;
  margin-left: auto;
  margin-right: auto;
  background: #f6f6f6;
  padding: 30px 0px 0px 0px;
  /* margin-bottom: 20vh; */
  @media (max-width: 620px) {
    width: 95%;
  }
`;

export default LayoutDashboard;
