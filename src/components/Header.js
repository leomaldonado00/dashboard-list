import React from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import { BiLogOut } from "react-icons/bi";
import { BsListUl } from "react-icons/bs";
import { IoMdAddCircleOutline } from "react-icons/io";
import { logout } from "../ducks/access";


const Header = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();

  function logout0() {
    dispatch(logout());
    localStorage.removeItem("token");
    history.push("/");
  }

  return (
    <div>
      <HeaderDivMov bg={props.bg}>
        <Toggle title="Salir" onClick={() => logout0()}>
          <BiLogOut size={28} color="#fff" />
        </Toggle>

        <WrapArrowLogo>
          <Title color="#fff" margin="auto -30px auto auto" fontWeight="600">
            ACME
          </Title>
        </WrapArrowLogo>

        <Notif
          title="Crear nuevo Dashboard"
          onClick={() => history.push(`/create-dashboard`)}
          marginRight="15px"
        >
          <IoMdAddCircleOutline size={28} color="white" />
        </Notif>

        <Notif
          title="Lista de dashboard"
          onClick={() => history.push(`/dashboard-list`)}
        >
          <BsListUl size={28} color="white" />
        </Notif>
      </HeaderDivMov>
    </div>
  );
};

const WrapArrowLogo = styled.div`
  display: flex;
  margin: auto; /* center slf */
`;
const HeaderDivMov = styled.div`
  display: flex;
  position: fixed;
  top: 0px;
  z-index: 999999;
  width: 100%;
  background: ${(props) => props.bg || "silver"};
  height: 64px;
  align-items: center;
`;

const Toggle = styled.div`
  cursor: pointer;
  // position:absolute;  /* left slf */
  width: auto;
  height: 40px;
  display: flex;
  align-items: center;
  margin-left: 80px;
  @media (max-width: 620px) {
    margin-left: 10px;
  }
`;
const Notif = styled.div`
  cursor: pointer;
  // position:absolute;  /* left slf */
  width: auto;
  height: 40px;
  display: flex;
  align-items: center;
  margin-right: ${(props) => props.marginRight || "80px"};
  @media (max-width: 620px) {
    margin-right: 10px;
  }
`;
const Title = styled.div`
  font-family: Montserrat, sans-serif;
  font-size: ${(props) => props.fontSize || "28px"};
  font-weight: ${(props) => props.fontWeight || "300"};
  line-height: 1.71;
  text-align: center;
  color: ${(props) => props.color || "silver"};
  margin: ${(props) => props.margin || "auto"};
`;

export default Header;
